#-------------------------------------------------
#
# Project created by QtCreator 2014-02-04T15:08:17
#
#-------------------------------------------------

QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TuxInfo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ayuda.cpp

HEADERS  += mainwindow.h \
    ayuda.h

FORMS    += mainwindow.ui \
    ayuda.ui

RESOURCES += \
    Iconos.qrc
