#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ayuda.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->on_actionNuevaPestana_triggered();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionInicio_triggered()
{
    emit homeClicked();
}

void MainWindow::on_actionAtras_triggered()
{
    emit atrasClicked();
}

void MainWindow::on_actionNuevaPestana_triggered()
{
    Ayuda *ayuda = new Ayuda();
    ui->mdiArea->addSubWindow(ayuda);
    ayuda->setWindowState(Qt::WindowMaximized);
    ayuda->show();
    connect(this, SIGNAL(atrasClicked()),ayuda, SLOT(atras()));
    connect(this, SIGNAL(adelanteClicked()), ayuda, SLOT(adelante()));
    connect(ayuda, SIGNAL(windowTitleChanged(QString)),this, SLOT(cambiarTitulo(QString)));
}

void MainWindow::on_actionAdelante_triggered()
{
    emit adelanteClicked();
}

void MainWindow::cambiarTitulo(QString title)
{
    this->setWindowTitle("INFOTUX |" + title);
}


void MainWindow::on_actionAcerca_de_QT_triggered()
{
    QMessageBox::aboutQt(this);
}
