#include "ayuda.h"
#include "ui_ayuda.h"
#include <QUrl>

Ayuda::Ayuda(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Ayuda)
{
    ui->setupUi(this);
    ui->webView->setUrl(*new QUrl("http://blog.desdelinux.net/te-gustaria-probar-linux-guia-para-curiosos-y-recien-llegados-2da-parte/"));
    connect(ui->webView, SIGNAL(titleChanged(QString)), this, SLOT(cambiarTitulo()));
}

Ayuda::~Ayuda()
{
    delete ui;
}

void Ayuda::atras()
{
    ui->webView->back();
}

void Ayuda::adelante()
{
    ui->webView->forward();
}

void Ayuda::home()
{
    ui->webView->setUrl(*new QUrl("http://www.desdelinux.net"));
}

void Ayuda::cambiarTitulo()
{
    QString title = ui->webView->title();
    title.remove(26,(title.length()-26));
    this->setWindowTitle(title);
    qDebug()<< "Cambiando Titulo: " << ui->webView->title();
}
