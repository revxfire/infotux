#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <ayuda.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void atrasClicked();
    void adelanteClicked();
    void homeClicked();

public slots:
    void cambiarTitulo(QString title);

private slots:
    void on_actionInicio_triggered();

    void on_actionAtras_triggered();

    void on_actionNuevaPestana_triggered();

    void on_actionAdelante_triggered();



    void on_actionAcerca_de_QT_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
